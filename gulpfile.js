const gulp = require('gulp');
const download = require('gulp-download');
const rename = require('gulp-rename');
const done = function () { };

// meyer-reset
gulp.task('meyer', function (done) {
    download('https://meyerweb.com/eric/tools/css/reset/reset.css')
        .pipe(rename('_meyer-reset.scss'))
        .pipe(gulp.dest('./resets'));
    done();
});

gulp.task('modern', function (done) {
    gulp
        .src('./node_modules/modern-css-reset/dist/reset.css')
        .pipe(rename('_modern-reset.scss'))
        .pipe(gulp.dest('./resets/'));
    done();
});

gulp.task('normalize', function (done) {
    gulp
        .src('./node_modules/normalize.css/normalize.css')
        .pipe(rename('_normalize.scss'))
        .pipe(gulp.dest('./resets/'));
    done();
});

gulp.task('sanitize', function (done) {
    gulp
        .src('./node_modules/sanitize.css/sanitize.css')
        .pipe(rename('_sanitize.scss'))
        .pipe(gulp.dest('./resets/'));
    done();
});

gulp.task('sanitize-typo', function (done) {
    gulp
        .src('./node_modules/sanitize.css/typography.css')
        .pipe(rename('_sanitize-typography.scss'))
        .pipe(gulp.dest('./resets/'));
    done();
});

gulp.task('sanitize-form', function (done) {
    gulp
        .src('./node_modules/sanitize.css/forms.css')
        .pipe(rename('_sanitize-forms.scss'))
        .pipe(gulp.dest('./resets/'));
    done();
});

gulp.task(
    'sanitize-all',
    gulp.parallel('sanitize', 'sanitize-typo', 'sanitize-form')
);

gulp.task('reboot', function (done) {
    gulp
        .src('./node_modules/bootstrap/dist/css/bootstrap-reboot.css')
        .pipe(rename('_reboot.scss'))
        .pipe(gulp.dest('./resets/'));
    done();
});
gulp.task(
    'default',
    gulp.parallel('meyer', 'modern', 'normalize', 'sanitize-all', 'reboot')
);