# scss-resets

Choice of css resets. The following are available

- [meyer-reset](https://meyerweb.com/eric/tools/css/reset/)
- [modern-css-reset](hhttps://github.com/hankchizljaw/modern-css-reset)
- [normalize](https://necolas.github.io/normalize.css/)
- [sanitize](https://csstools.github.io/sanitize.css/)
- [sanitize-forms](https://csstools.github.io/sanitize.css/)
- [sanitize-typography](https://csstools.github.io/sanitize.css/)
- [reboot](https://github.com/twbs/bootstrap/blob/v4-dev/scss/_reboot.scss)

### Install

```
npm install scss-resets --save-dev
```

OR

```
yarn add scss-resets --dev
```

## Using with [gulp-sass](https://github.com/dlmanning/gulp-sass)

```javascript
var gulp = require("gulp");
var sass = require("gulp-sass");

gulp.task("sass", function() {
  gulp
    .src("path/to/app.scss")
    .pipe(
      sass({
        includePaths: require("scss-resets").includePaths
      })
    )
    .pipe(gulp.dest("path/to/output.css"));
});
```

## Using with [webpack](https://webpack.js.org/loaders/sass-loader/)

```javascript
module.exports = {
  module: {
    rules: [
      {
        use: [
          {
            loader: "sass-loader",
            options: {
              includePaths: require("scss-resets").includePaths
            }
          }
        ]
      }
    ]
  }
};
```

## Using in scss

After creating your gulp task, you can import any of the resets like so:

```
@import 'meyer-reset';
```

```
@import 'modern-reset';
```

```
@import 'normalize';
```

```
@import 'sanitize';
```

```
@import 'sanitize-forms';
```

```
@import 'sanitize-typography';
```

```
@import 'reboot';
```
